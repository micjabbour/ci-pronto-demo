#include "integeradder.h"

IntegerAdder::IntegerAdder(int a, int b) : a(a), b(b) {}

int IntegerAdder::getSum() const { return a + b; }
