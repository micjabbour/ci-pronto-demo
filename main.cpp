#include "integeradder.h"

int main() {
  IntegerAdder ia(1, 2);

  bool success = ia.getSum() == 3;
  if (success)
    return 0;
  else
    return 1;
}
