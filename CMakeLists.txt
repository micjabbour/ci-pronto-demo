cmake_minimum_required(VERSION 3.7)

project("Integer Adder CI demo")

enable_testing()

add_executable(adder main.cpp
                     integeradder.cpp)

add_test(adderTest adder)
