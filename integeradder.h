class IntegerAdder {
public:
  IntegerAdder(int a, int b);
  int getSum() const;

private:
  int a, b;
};
